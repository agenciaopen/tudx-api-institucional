﻿USE TudXInstitucional

DELETE FROM dbo.CaracteristicaCategorizado
DBCC CHECKIDENT ('dbo.CaracteristicaCategorizado', RESEED, 0);
DELETE FROM dbo.ImagemCategorizado
DBCC CHECKIDENT ('dbo.ImagemCategorizado', RESEED, 0);
DELETE FROM dbo.ProdutoCategorizado
DBCC CHECKIDENT ('dbo.ProdutoCategorizado', RESEED, 0);
DELETE FROM dbo.Caracteristica
DBCC CHECKIDENT ('dbo.Caracteristica', RESEED, 0);
DELETE FROM ProdutoCaracteristicaNaoCategorizada
DBCC CHECKIDENT ('dbo.ProdutoCaracteristicaNaoCategorizada', RESEED, 0);
DELETE FROM CaracteristicaNaoCategorizada
DBCC CHECKIDENT ('dbo.CaracteristicaNaoCategorizada', RESEED, 0);
DELETE FROM dbo.CategoriaLoja
DBCC CHECKIDENT ('dbo.CategoriaLoja', RESEED, 0);
DELETE FROM dbo.Loja
DBCC CHECKIDENT ('dbo.Loja', RESEED, 0);
DELETE FROM dbo.Categoria
DBCC CHECKIDENT ('dbo.Categoria', RESEED, 0);
