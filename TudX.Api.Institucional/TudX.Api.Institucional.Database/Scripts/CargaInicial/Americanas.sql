﻿USE TudXInstitucional

SET IDENTITY_INSERT Loja ON;

Insert into Loja (Identificador, Nome, UrlLoja, UrlLogo)
values (2, 'Americanas', 'https://www.americanas.com.br/', 'https://olist.com/wp-content/uploads/2018/04/marketplace-americanas-2.png');

SET IDENTITY_INSERT Loja OFF;