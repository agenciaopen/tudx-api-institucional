﻿USE TudXCrawler

SET IDENTITY_INSERT Loja ON;

Insert into Loja (Identificador, Nome, UrlLoja, UrlLogo)
values (1, 'Shoptime', 'https://www.shoptime.com.br/', 'https://static.pontoslivelo.com.br/content/img/parceiros-de-pontos/shoptime-marca.png')

SET IDENTITY_INSERT Loja OFF;

SET IDENTITY_INSERT CategoriaLoja ON;

Insert into CategoriaLoja (Identificador,Nome,IdentificadorLoja,IdentificadorSuperior,IdentificadorCategoria)
values (1,'Celulares e Smartphones', 1, null, 1),
	   (2,'acessórios para celular',1, 1, 2),
	   (3,'bateria para celular', 1, 2, 3),
	   (4,'cabo de dados para celular',1, 2, 4),
	   (5,'peças para celular',1, 1, 5),
	   (6,'display', 1, 5, 6),
	   (7,'camera traseira', 1, 5, 7),
	   (8,'Eletrodomésticos', 1, null, 8),
	   (9,'Churrasqueira', 1, 8, 9),
	   (10,'churrasqueira a gás', 1, 9, 10);

SET IDENTITY_INSERT CategoriaLoja OFF;

SET IDENTITY_INSERT Caracteristica ON;

Insert into Caracteristica (Identificador, Nome, IdentificadorCategoria, IdentificadorSuperior)
Values (1, 'Código', 3, null),
	   (2, 'Código de barras', 3, null);

Insert into Caracteristica (Identificador, Nome, IdentificadorCategoria, IdentificadorSuperior)
Values (3, 'Código', 4, null),
	   (4, 'Código de barras', 4, null),
	   (5, 'Marca', 4, null),
	   (6, 'Conteúdo da embalagem', 4, null),
	   (7, 'Alimentação', 4, null);

Insert into Caracteristica (Identificador, Nome, IdentificadorCategoria, IdentificadorSuperior)
Values (8, 'Código', 6, null),
	   (9, 'Código de barras', 6, null),
	   (10, 'Peso', 6, null),
	   (11, 'Nbm', 6, null);

Insert into Caracteristica (Identificador, Nome, IdentificadorCategoria, IdentificadorSuperior)
Values (12, 'Código', 7, null),
	   (13, 'Código de barras', 7, null),
	   (14, 'Fabricante', 7, null);

Insert into Caracteristica (Identificador, Nome, IdentificadorCategoria, IdentificadorSuperior)
Values (15, 'Código', 10, null),
	   (16, 'Código de barras', 10, null),
	   (17, 'Fabricante', 10, null),
	   (18, 'Peso', 10, null),
	   (19, 'Marca', 10, null);

SET IDENTITY_INSERT Caracteristica OFF;