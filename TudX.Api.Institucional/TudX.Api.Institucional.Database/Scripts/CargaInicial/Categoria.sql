﻿USE TudXInstitucional

SET IDENTITY_INSERT Categoria ON;

Insert into Categoria (Identificador,Nome,IdentificadorSuperior)
values (1,'Celulares e Smartphones', null),
	   (2,'acessórios para celular',1),
	   (3,'bateria para celular',2),
	   (4,'cabo de dados para celular',2),
	   (5,'peças para celular',1),
	   (6,'display',5),
	   (7,'camera traseira',5),
	   (8,'Eletrodomésticos', null),
	   (9,'Churrasqueira',8),
	   (10,'churrasqueira a gás',9);

SET IDENTITY_INSERT Categoria OFF;