﻿CREATE TABLE [dbo].[CategoriaLoja] (
    [Identificador]          BIGINT        IDENTITY (1, 1) NOT NULL,
    [Nome]                   VARCHAR (255) NOT NULL,
    [IdentificadorLoja]      BIGINT        NOT NULL,
    [IdentificadorSuperior]  BIGINT        NULL,
    [IdentificadorCategoria] BIGINT        NULL,
    CONSTRAINT [PK_CategoriaLoja] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_CategoriaLoja_Categoria] FOREIGN KEY ([IdentificadorCategoria]) REFERENCES [dbo].[Categoria] ([Identificador]),
    CONSTRAINT [FK_CategoriaLoja_CategoriaLoja] FOREIGN KEY ([IdentificadorSuperior]) REFERENCES [dbo].[CategoriaLoja] ([Identificador]),
    CONSTRAINT [FK_CategoriaLoja_Loja] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[Loja] ([Identificador])
);

