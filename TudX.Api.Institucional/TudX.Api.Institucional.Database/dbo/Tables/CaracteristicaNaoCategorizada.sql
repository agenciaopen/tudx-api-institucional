﻿CREATE TABLE [dbo].[CaracteristicaNaoCategorizada] (
    [Identificador]          BIGINT       IDENTITY (1, 1) NOT NULL,
    [Nome]                   VARCHAR (50) NOT NULL,
    [QuantidadeIncidencia]   INT          NOT NULL,
    [FoiAdicionada]          BIT          NOT NULL,
    [IdentificadorCategoria] BIGINT       NOT NULL,
    CONSTRAINT [PK_CaracteristicaNaoCategorizada] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_CaracteristicaNaoCategorizada_Categoria] FOREIGN KEY ([IdentificadorCategoria]) REFERENCES [dbo].[Categoria] ([Identificador])
);

