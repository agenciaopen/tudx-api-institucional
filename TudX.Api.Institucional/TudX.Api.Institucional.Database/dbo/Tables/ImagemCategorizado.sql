﻿CREATE TABLE [dbo].[ImagemCategorizado] (
    [Identificador]        BIGINT        IDENTITY (1, 1) NOT NULL,
    [UrlImagem]            VARCHAR (MAX) NOT NULL,
    [IdentificadorProduto] BIGINT        NOT NULL,
    CONSTRAINT [PK_ImagemCategorizado] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ImagemCategorizado_ProdutoCategorizado] FOREIGN KEY ([IdentificadorProduto]) REFERENCES [dbo].[ProdutoCategorizado] ([Identificador])
);



