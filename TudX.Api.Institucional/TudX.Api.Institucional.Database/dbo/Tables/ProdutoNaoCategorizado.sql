﻿CREATE TABLE [dbo].[ProdutoNaoCategorizado] (
    [Identificador]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [Nome]              VARCHAR (255) NOT NULL,
    [MigalhaCategoria]  VARCHAR (255) NOT NULL,
    [DataInclusao]      DATETIME      NOT NULL,
	[DataAlteracao]     DATETIME      NULL,
    [IdentificadorLoja] BIGINT        NOT NULL,
    [UrlAnuncio] VARCHAR(MAX) NOT NULL, 
    CONSTRAINT [PK_ProdutoNaoCategorizado] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ProdutoNaoCategorizado_Loja] FOREIGN KEY ([IdentificadorLoja]) REFERENCES [dbo].[Loja] ([Identificador])
);

