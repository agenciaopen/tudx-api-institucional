﻿CREATE TABLE [dbo].[EventoEnviado] (
    [Identificador]       BIGINT           IDENTITY (1, 1) NOT NULL,
    [IdentificadorEvento] UNIQUEIDENTIFIER NOT NULL,
    [Nome]                VARCHAR (255)    NOT NULL,
    [DataEnvio]           DATETIME         NOT NULL,
    [Mensagem]            VARCHAR (MAX)    NOT NULL,
    [QuantidadeEnvio]     INT              NOT NULL,
    CONSTRAINT [PK_EventoEnviado] PRIMARY KEY CLUSTERED ([Identificador] ASC)
);

