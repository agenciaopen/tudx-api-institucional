﻿CREATE TABLE [dbo].[ProdutoCategorizado] (
    [Identificador]               BIGINT          IDENTITY (1, 1) NOT NULL,
    [IdentificadorCategoria]      BIGINT          NOT NULL,
    [IdentificadorLoja]           BIGINT          NOT NULL,
    [CodigoProduto]               VARCHAR (50)    NULL,
    [Nome]                        VARCHAR (255)   NOT NULL,
    [Descricao]                   TEXT            NOT NULL,
    [UrlAnuncio]                  VARCHAR (MAX)   NOT NULL,
    [Valor]                       DECIMAL (18, 2) NOT NULL,
    [FormaPagamento]              VARCHAR (50)    NOT NULL,
    [IdentificadorProdutoCrawler] BIGINT          NOT NULL,
    CONSTRAINT [PK_ProdutoCategorizado] PRIMARY KEY CLUSTERED ([Identificador] ASC)
);



