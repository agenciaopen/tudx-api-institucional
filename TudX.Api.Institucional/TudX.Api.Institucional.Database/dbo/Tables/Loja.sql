﻿CREATE TABLE [dbo].[Loja] (
    [Identificador] BIGINT        IDENTITY (1, 1) NOT NULL,
    [Nome]          VARCHAR (255) NOT NULL,
    [UrlLoja]       VARCHAR (MAX) NOT NULL,
    [UrlLogo]       VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_Loja] PRIMARY KEY CLUSTERED ([Identificador] ASC)
);



