﻿CREATE TABLE [dbo].[ProdutoCaracteristicaNaoCategorizada] (
    [Identificador]                              BIGINT   IDENTITY (1, 1) NOT NULL,
    [IdentificadorCaracteristicaNaoCategorizada] BIGINT   NOT NULL,
    [IdentificadorProdutoCategorizado]           BIGINT   NOT NULL,
    [DataInclusao]                               DATETIME NOT NULL,
    CONSTRAINT [PK_ProdutoCaracteristicaNaoCategorizada] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ProdutoCaracteristicaNaoCategorizada_CaracteristicaNaoCategorizada] FOREIGN KEY ([IdentificadorCaracteristicaNaoCategorizada]) REFERENCES [dbo].[CaracteristicaNaoCategorizada] ([Identificador]),
    CONSTRAINT [FK_ProdutoCaracteristicaNaoCategorizada_ProdutoCategorizado] FOREIGN KEY ([IdentificadorProdutoCategorizado]) REFERENCES [dbo].[ProdutoCategorizado] ([Identificador])
);

