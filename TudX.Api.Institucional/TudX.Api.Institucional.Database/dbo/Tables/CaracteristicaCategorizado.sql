﻿CREATE TABLE [dbo].[CaracteristicaCategorizado] (
    [Identificador]               BIGINT        IDENTITY (1, 1) NOT NULL,
    [Nome]                        VARCHAR (50)  NOT NULL,
    [Valor]                       VARCHAR (255) NULL,
    [IdentificadorProduto]        BIGINT        NOT NULL,
    [IdentificadorCaracteristica] BIGINT        NOT NULL,
    CONSTRAINT [PK_CaracteristicaCategorizado] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_CaracteristicaCategorizado_Caracteristica] FOREIGN KEY ([IdentificadorCaracteristica]) REFERENCES [dbo].[Caracteristica] ([Identificador]),
    CONSTRAINT [FK_CaracteristicaCategorizado_ProdutoCategorizado] FOREIGN KEY ([IdentificadorProduto]) REFERENCES [dbo].[ProdutoCategorizado] ([Identificador])
);

