﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog;
using TudX.Api.Base.Controller;
using TudX.Api.Institucional.Application.Services;
using TudX.Api.Institucional.CrossCuting.Data;
using TudX.Api.Institucional.Domain.Model;
using TudX.Api.Institucional.IntegrationEvents.Events;
using TudX.Domain.Base.Repository;
using TudX.Exceptions;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Institucional.Controllers
{
    /// <summary>
    /// Fornece funcionalidades referentes a entidade Categoria.
    /// </summary>
    [ApiVersion("1.0")]
    public class CategoriaController : TudxApiController
    {
        private readonly CategoriaService categoriaService;
        private readonly IEventBus eventBus;

        /// <summary>
        /// Construtor padrão.
        /// </summary>
        public CategoriaController(IServiceProvider serviceProvider,
                                   IEventBus eventBus)
        {
            categoriaService = new CategoriaService(serviceProvider);
            this.eventBus = eventBus;
        }

        /// <summary>
        /// Recupera todas as categorias.
        /// </summary>
        /// <response code="200">Dados de todas as categorias</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        [HttpGet]
        [ProducesResponseType(typeof(CategoriaData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> BuscarTodas()
        {
            var categorias = categoriaService.BuscarTodasCategorias();

            return Ok(categorias.Adapt<List<CategoriaData>>());
        }

        /// <summary>
        /// Recupera a categoria por identificador.
        /// </summary>
        /// <response code="200">Categoria do identificador</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        [HttpGet]
        [Route("{identificador}")]
        [ProducesResponseType(typeof(CategoriaData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> BuscarPorIdentificador(long identificador)
        {
            var categoria = categoriaService.BuscarCategoriaPorIdentificador(identificador);

            return Ok(categoria.Adapt<CategoriaData>());
        }

        /// <summary>
        /// Salva a categoria.
        /// </summary>
        /// <response code="200">Dados da categoria</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        /// <param name="categoriaData">Dados da categoria.</param>
        /// <returns>Dados da categoria.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(CategoriaParaSalvarData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> Salvar([FromBody] CategoriaParaSalvarData categoriaData)
        {
            bool isUpdate = categoriaData.Identificador > 0;
            var categoria = categoriaService.Salvar(categoriaData);
            CriarMensagemDaCategoria(categoria, isUpdate);

            return CreatedAtAction(nameof(BuscarPorIdentificador), new { identificador = categoria.Identificador }, categoria);
        }

        /// <summary>
        /// Salva a categoria.
        /// </summary>
        /// <response code="200">Dados da categoria para carga</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        /// <param name="categoriaData">Dados da categoria.</param>
        /// <returns>Dados da loja inserida.</returns>
        [HttpPost("Carga")]
        [ProducesResponseType(typeof(CategoriaParaCargaData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<ActionResult<bool>> SalvarCarga([FromBody] CategoriaParaCargaData categoriaData)
        {
            Categoria categoria = categoriaService.SalvarCarga(categoriaData.Adapt<Categoria>());
            CriarMensagemDaCargaDeCategoria(categoria, false);

            return true;
        }

        /// <summary>
        /// Salva varias categorias.
        /// </summary>
        /// <response code="200">Dados das Categorias</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        /// <param name="categoriasData">Dados das categorias.</param>
        /// <returns>Dados das categorias.</returns>
        [HttpPost("CargaLista")]
        [ProducesResponseType(typeof(CategoriaParaCargaData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<ActionResult<bool>> SalvarCargaLista([FromBody] List<CategoriaParaCargaData> categoriasData)
        {
            foreach (var categoriaData in categoriasData)
            {
                Categoria categoria = categoriaService.SalvarCarga(categoriaData.Adapt<Categoria>());
                CriarMensagemDaCargaDeCategoria(categoria, false);
            }

            return true;
        }

        private void CriarMensagemDaCargaDeCategoria(Categoria categoria, bool isUpdate)
        {
            if (isUpdate)
            {
                CargaCategoriaAlteradaIntegrationEvent message = categoria.Adapt<CargaCategoriaAlteradaIntegrationEvent>();
                eventBus.PublishAndRegisterEvent(message);
            }
            else
            {
                CargaCategoriaAdicionadaIntegrationEvent message = categoria.Adapt<CargaCategoriaAdicionadaIntegrationEvent>();
                eventBus.PublishAndRegisterEvent(message);
            }
        }

        private void CriarMensagemDaCategoria(Categoria categoria, bool isUpdate)
        {
            if (isUpdate)
            {
                CategoriaAlteradaIntegrationEvent message = categoria.Adapt<CategoriaAlteradaIntegrationEvent>();
                eventBus.PublishAndRegisterEvent(message);
            }
            else
            {
                CategoriaAdicionadaIntegrationEvent message = categoria.Adapt<CategoriaAdicionadaIntegrationEvent>();
                eventBus.PublishAndRegisterEvent(message);
            }
        }
    }
}