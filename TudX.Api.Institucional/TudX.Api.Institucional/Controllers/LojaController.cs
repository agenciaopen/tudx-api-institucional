﻿using Mapster;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Base.Controller;
using TudX.Api.Institucional.Application.Services;
using TudX.Api.Institucional.CrossCuting.Data;
using TudX.Api.Institucional.Domain.Model;
using TudX.Api.Institucional.IntegrationEvents.Events;
using TudX.Exceptions;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Institucional.Controllers
{
    /// <summary>
    /// Fornece funcionalidades referentes a entidade Loja.
    /// </summary>
    [ApiVersion("1.0")]
    public class LojaController : TudxApiController
    {
        private readonly LojaService lojaService;
        private readonly IEventBus eventBus;

        /// <summary>
        /// Construtor padrão
        /// </summary>
        /// <param name="serviceProvider">Provedor de serviços</param>
        /// <param name="eventBus">Barramento de eventos/mensagens.</param>
        public LojaController(IServiceProvider serviceProvider,
                              IEventBus eventBus)
        {
            lojaService = new LojaService(serviceProvider);
            this.eventBus = eventBus;
        }

        /// <summary>
        /// Recupera todas as lojas.
        /// </summary>
        /// <response code="200">Dados de todas as lojas</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        [HttpGet]
        [ProducesResponseType(typeof(List<LojaData>), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> BuscarTodas()
        {
            var lojas = lojaService.BuscarTodasAsLojas();

            return Ok(lojas);
        }

        /// <summary>
        /// Recupera a loja por identificador.
        /// </summary>
        /// <response code="200">Loja do identificador</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        [HttpGet("{identificador}")]
        [ProducesResponseType(typeof(LojaData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> BuscarPorIdentificador(long identificador)
        {
            var loja = lojaService.BuscarLojaPorIdentificador(identificador);

            return Ok(loja);
        }

        /// <summary>
        /// Salva a loja.
        /// </summary>
        /// <response code="200">Dados da loja</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        /// <param name="lojaData">Dados da loja.</param>
        /// <returns>Dados da loja.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(CategoriaParaSalvarData), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> Salvar([FromBody] LojaData lojaData)
        {
            bool isUpdate = lojaData.Identificador > 0;

            var loja = lojaService.Salvar(lojaData);
            CriarMensagemDaLoja(loja, isUpdate);

            return CreatedAtAction(nameof(BuscarPorIdentificador), new { identificador = loja.Identificador }, loja);
        }

        private void CriarMensagemDaLoja(Loja loja, bool isUpdate)
        {
            if (isUpdate)
            {
                LojaAlteradaIntegrationEvent message = loja.Adapt<LojaAlteradaIntegrationEvent>();
                eventBus.PublishAndRegisterEvent(message);
            }
            else
            {
                LojaAdicionadaIntegrationEvent message = loja.Adapt<LojaAdicionadaIntegrationEvent>();
                eventBus.PublishAndRegisterEvent(message);
            }
        }
    }
}
