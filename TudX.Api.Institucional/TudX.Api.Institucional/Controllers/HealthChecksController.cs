﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Base.Controller;

namespace TudX.Api.Institucional.Controllers
{
    /// <summary>
    /// Verificador de saúde da api.
    /// </summary>
    [ApiVersion("1.0")]
    public class HealthChecksController : TudxHealthChecksController
    {
    }
}
