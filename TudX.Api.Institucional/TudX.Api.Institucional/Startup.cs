﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TudX.Api.Base;
using TudX.Api.Base.Metadata;
using TudX.Api.Institucional.Infra.NH.DependencyInjection;
using TudX.IntegrationEvents.EventBus.Abstractions;
using TudX.Adapter.Eventbus.RabbitMQ.Extensions;
using TudX.Api.Institucional.IntegrationEvents.Events;
using TudX.Api.Institucional.IntegrationEvents.EventHandling;
using MediatR;
using TudX.Api.Institucional.Application.DomainEvents.EventHandling;

namespace TudX.Api.Institucional
{
    /// <summary>
    /// Configuração de inicio da aplicação.
    /// </summary>
    public class Startup : TudxApiBaseStartup
    {
        /// <summary>
        /// Informações a serem apresentadas na documentação do swagger.
        /// </summary>
        protected override ApiMetadata ApiMetadata => new ApiMetadata()
        {
            Name = "Api Institucional",
            Description = "Esta api será utilizada para configurar dados institucionais do TudX.",
            DefaultApiVersion = "1.0"
        };

        /// <summary>
        /// Construtor.
        /// </summary>
        /// <param name="configuration">Informações de configurações da aplicação.</param>
        public Startup(IConfiguration configuration) : base(configuration)
        {
            
        }

        /// <summary>
        /// Métodos de configurações de IoC
        /// </summary>
        /// <param name="services">Classe responsável por configurar o DI.</param>
        protected override void ConfigureApiServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.AddCors(options =>
            {
                options.AddPolicy("TudXPolicy",
                builder =>
                {
                    builder.WithOrigins("http://tudx01",
                                        "http://localhost",
                                        "http://localhost:4200")
                                        .AllowAnyHeader()
                                        .AllowAnyMethod()
                                        .AllowCredentials();
                });
            });

            services.AddNHAdapter(Configuration);
            services.AddRabbitMQAdapter(Configuration);
            services.AddMediatR(typeof(CaracteristicaNaoCategorizadaDomainEventHandler).Assembly);

            services.AddTransient<ProdutoCrawlerAdicionadoIntegrationEventHandler>();
            services.AddTransient<ProdutoCrawlerAlteradoIntegrationEventHandler>();
        }

        /// <summary>
        /// Método de configuração da aplicação.
        /// </summary>
        /// <param name="app">Construtor da aplicação.</param>
        protected override void ConfigureApi(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

            eventBus.Subscribe<ProdutoCrawlerAdicionadoIntegrationEvent, ProdutoCrawlerAdicionadoIntegrationEventHandler>();
            eventBus.Subscribe<ProdutoCrawlerAlteradoIntegrationEvent, ProdutoCrawlerAlteradoIntegrationEventHandler>();

            app.UseCors("TudXPolicy");
            app.UseRouting();
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}
