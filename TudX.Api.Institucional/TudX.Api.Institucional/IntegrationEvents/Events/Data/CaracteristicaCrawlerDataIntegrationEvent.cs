﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Institucional.IntegrationEvents.Events.Data
{
    public class CaracteristicaCrawlerDataIntegrationEvent
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string Valor { get; set; }
    }
}
