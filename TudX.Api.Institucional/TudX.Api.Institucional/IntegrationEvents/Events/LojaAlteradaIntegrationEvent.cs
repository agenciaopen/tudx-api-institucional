﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.IntegrationEvents.Events;

namespace TudX.Api.Institucional.IntegrationEvents.Events
{
    public class LojaAlteradaIntegrationEvent : IntegrationEvent
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string UrlLoja { get; set; }
        public string UrlLogo { get; set; }
    }
}
