﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Institucional.IntegrationEvents.Events.Data;
using TudX.IntegrationEvents.Events;

namespace TudX.Api.Institucional.IntegrationEvents.Events
{
    public class ProdutoCategorizadoIntegrationEvent : IntegrationEvent
    {
        public long Identificador { get; set; }
        public string CodigoProduto { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string UrlAnuncio { get; set; }
        public decimal Valor { get; set; }
        public string FormaPagamento { get; set; }
        public long IdentificadorLoja { get; set; }
        public long IdentificadorCategoria { get; set; }
        public long IdentificadorProdutoCrawler { get; set; }
        public ICollection<CaracteristicaCategorizadoDataIntegrationEvent> Caracteristicas { get; set; }
        public ICollection<ImagemCategorizadoDataIntegrationEvent> Imagens { get; set; }
    }
}
