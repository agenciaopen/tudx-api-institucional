﻿using Mapster;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TudX.Api.Institucional.Application.Services;
using TudX.Api.Institucional.CrossCuting.Data;
using TudX.Api.Institucional.Domain.Model;
using TudX.Api.Institucional.IntegrationEvents.Events;
using TudX.Api.Institucional.IntegrationEvents.Events.Data;
using TudX.Domain.Base.Repository;
using TudX.IntegrationEvents.Application.Service;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Institucional.IntegrationEvents.EventHandling
{
    public class ProdutoCrawlerAlteradoIntegrationEventHandler : IIntegrationEventHandler<ProdutoCrawlerAlteradoIntegrationEvent>
    {
        private readonly ProdutoCategorizadoService produtoCategorizadoService;
        private readonly EventoRecebidoService eventoRecebidoService;
        private readonly IEventBus eventBus;
        private readonly ILogger _logger;

        public ProdutoCrawlerAlteradoIntegrationEventHandler(IServiceProvider serviceProvider,
                                                             ILoggerFactory loggerFactory,
                                                             IEventBus eventBus,
                                                             IMediator mediator)
        {
            produtoCategorizadoService = new ProdutoCategorizadoService(serviceProvider, mediator);
            eventoRecebidoService = new EventoRecebidoService(serviceProvider);
            _logger = loggerFactory?.CreateLogger<ProdutoCrawlerAlteradoIntegrationEventHandler>() ?? throw new ArgumentNullException(nameof(loggerFactory));
            this.eventBus = eventBus;
        }

        public async Task Handle(ProdutoCrawlerAlteradoIntegrationEvent @event)
        {
            try
            {
                ProdutoCategorizado produtoCategorizado = await produtoCategorizadoService.AlterarProduto(@event.Adapt<ProdutoCrawlerData>());
                if (produtoCategorizado != null)
                {
                    eventoRecebidoService.RegistrarRecebimentoDeEvento(@event);
                    ProdutoCategorizadoIntegrationEvent message = produtoCategorizado.Adapt<ProdutoCategorizadoIntegrationEvent>();
                    eventBus.PublishAndRegisterEvent(message);

                    if (produtoCategorizado.Caracteristicas != null && produtoCategorizado.Caracteristicas.Count > 0)
                    {
                        InformacaoCaracteristicaProdutoIntegrationEvent messageCaracteristica = new InformacaoCaracteristicaProdutoIntegrationEvent();
                        messageCaracteristica.IdentificadorCategoria = produtoCategorizado.IdentificadorCategoria;
                        messageCaracteristica.CaracteristicasComValor = produtoCategorizado.Caracteristicas.Adapt<List<CaracteristicaCategorizadoDataIntegrationEvent>>();

                        eventBus.PublishAndRegisterEvent(messageCaracteristica);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Ocorreu um erro ao processar a mensagem: {0} Stack: {1}", ex.Message, ex.StackTrace));
            }
        }
    }
}
