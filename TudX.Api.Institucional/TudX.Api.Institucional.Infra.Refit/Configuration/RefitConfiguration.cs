﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Institucional.Infra.Refit.Configuration
{
    public class RefitConfiguration
    {
        public string UrlBaseApiInstitucional { get; set; }
    }
}
