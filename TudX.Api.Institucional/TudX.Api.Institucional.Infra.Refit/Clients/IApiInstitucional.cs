﻿using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TudX.Api.Institucional.Infra.Refit.Clients
{
    public interface IApiInstitucional
    {
        [Get("/healthz")]
        Task<string> HealthChecksAsync();
    }
}
