﻿using Microsoft.Extensions.DependencyInjection;
using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Infra.Refit.Clients;
using TudX.Api.Institucional.Infra.Refit.Configuration;
using TudX.Core.Abstractions;

namespace TudX.Api.Institucional.Infra.Refit.DependencyInjection
{
    public static class InfraRefitServiceCollectionExtensions
    {
        public static IServiceCollection AddRefitAdapter(this IServiceCollection services, RefitConfiguration refitConfiguration)
        {
            services.AddSingleton(refitConfiguration);

            // Configura os parametros para chamada na TMDb API e registra a interface ITmdbApi.
            services.AddScoped(serviceProvider =>
            {
                var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
                var httpClient = httpClientFactory.CreateHttpClient();
                httpClient.BaseAddress = new Uri(refitConfiguration.UrlBaseApiInstitucional);

                return RestService.For<IApiInstitucional>(httpClient);
            });

            return services;
        }
    }
}
