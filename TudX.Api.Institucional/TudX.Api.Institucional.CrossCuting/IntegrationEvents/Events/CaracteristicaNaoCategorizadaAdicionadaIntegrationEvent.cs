﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.IntegrationEvents.Events;

namespace TudX.Api.Institucional.CrossCuting.IntegrationEvents.Events
{
    public class CaracteristicaNaoCategorizadaAdicionadaIntegrationEvent : IntegrationEvent
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public long IdentificadorCategoria { get; set; }        
    }
}
