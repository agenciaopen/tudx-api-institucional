﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Core.Filters;

namespace TudX.Api.Institucional.CrossCuting.Data
{
    /// <summary>
    /// Dados das categorias
    /// </summary>
    public class CategoriaData
    {        
        public long Identificador { get; set; }
        public long? IdentificadorSuperior { get; set; }
        public string Nome { get; set; }
        [SwaggerExclude]
        public List<CategoriaData> CategoriasFilhas { get; set; }
    }
}
