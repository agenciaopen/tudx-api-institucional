﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Institucional.CrossCuting.Data
{
    public class CategoriaLojaParaCargaData
    {
        public string Nome { get; set; }
        public string NomeCategoriaTudx { get; set; }
        public List<CategoriaLojaParaCargaData> CategoriasFilhas { get; set; }
    }
}
