﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Institucional.CrossCuting.Data
{

    public class CategoriaParaSalvarData
    {
        public long Identificador { get; set; }
        public long? IdentificadorSuperior { get; set; }
        public string Nome { get; set; }
    }
}
