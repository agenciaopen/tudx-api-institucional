﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Institucional.CrossCuting.Data
{
    public class CargaCategoriaLojaData
    {
        public long IdentificadorLoja { get; set; }
        public CategoriaLojaParaCargaData Categoria { get; set; }
    }
}
