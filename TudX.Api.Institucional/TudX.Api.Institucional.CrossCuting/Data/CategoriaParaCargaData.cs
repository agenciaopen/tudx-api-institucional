﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Institucional.CrossCuting.Data
{
    /// <summary>
    /// Dados das categorias para carga de dados.
    /// </summary>
    public class CategoriaParaCargaData
    {
        public string Nome { get; set; }
        public List<CategoriaParaCargaData> CategoriasFilhas { get; set; }
    }
}
