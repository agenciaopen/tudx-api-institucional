﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Institucional.CrossCuting.Data
{
    /// <summary>
    /// Dados das caracteristicas do produto
    /// </summary>
    public class CaracteristicaCrawlerData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string Valor { get; set; }
    }
}
