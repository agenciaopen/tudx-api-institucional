﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Institucional.CrossCuting.Data
{
    public class LojaData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string UrlLoja { get; set; }
        public string UrlLogo { get; set; }
    }
}
