﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Institucional.CrossCuting.Data
{
    /// <summary>
    /// Dados e entrada das imagens do produto.
    /// </summary>
    public class ImagemCrawlerData
    {
        public long Identificador { get; set; }
        public string UrlImagem { get; set; }
    }
}
