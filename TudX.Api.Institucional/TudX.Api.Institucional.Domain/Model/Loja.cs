﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Institucional.Domain.Model
{
    public class Loja : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string Nome { get; set; }
        public virtual string UrlLoja { get; set; }
        public virtual string UrlLogo { get; set; }

        public Loja(string nome)
        {
            Nome = nome;
        }

        public Loja()
        {

        }
    }
}
