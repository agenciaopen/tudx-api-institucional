﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Institucional.Domain.Model
{
    public class ProdutoNaoCategorizado : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string Nome { get; set; }
        public virtual string MigalhaCategoria { get; set; }
        public virtual DateTime DataInclusao { get; set; }
        public virtual DateTime? DataAlteracao { get; set; }
        public virtual long IdentificadorLoja { get; set; }
        public virtual string UrlAnuncio { get; set; }
    }
}
