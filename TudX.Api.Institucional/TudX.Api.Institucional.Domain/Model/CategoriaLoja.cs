﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Institucional.Domain.Model
{
    public class CategoriaLoja : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string Nome { get; set; }
        public virtual long IdentificadorLoja { get; set; }
        public virtual long IdentificadorCategoria { get; set; }
        public virtual long? IdentificadorSuperior { get; set; }
        public virtual ICollection<CategoriaLoja> CategoriasFilhas { get; set; }

        public CategoriaLoja(string nome, long identificadorLoja, long identificadorCategoria)
        {
            Nome = nome;
            IdentificadorLoja = identificadorLoja;
            IdentificadorCategoria = identificadorCategoria;
        }

        public CategoriaLoja()
        {

        }
    }
}
