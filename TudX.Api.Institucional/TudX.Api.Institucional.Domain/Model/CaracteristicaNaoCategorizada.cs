﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Institucional.Domain.Model
{
    public class CaracteristicaNaoCategorizada : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string Nome { get; set; }
        public virtual int QuantidadeIncidencia { get; set; }
        public virtual bool FoiAdicionada { get; set; }
        public virtual long IdentificadorCategoria { get; set; }
        public virtual ICollection<ProdutoCaracteristicaNaoCategorizada> Produtos { get; set; }
    }
}
