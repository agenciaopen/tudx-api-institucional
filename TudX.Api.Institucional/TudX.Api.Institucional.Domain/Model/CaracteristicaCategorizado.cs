﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Institucional.Domain.Model
{
    public class CaracteristicaCategorizado : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Valor { get; set; }
        public virtual ProdutoCategorizado Produto { get; set; }
        public virtual long IdentificadorCaracteristica { get; set; }

        public CaracteristicaCategorizado(string nome, ProdutoCategorizado produto, long identificadorCaracteristica)
        {
            Nome = nome;
            Produto = produto;
            IdentificadorCaracteristica = identificadorCaracteristica;
        }

        public CaracteristicaCategorizado()
        {

        }
    }
}
