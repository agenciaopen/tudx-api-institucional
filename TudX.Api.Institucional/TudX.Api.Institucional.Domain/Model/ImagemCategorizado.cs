﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Institucional.Domain.Model
{
    public class ImagemCategorizado : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string UrlImagem { get; set; }
        public virtual ProdutoCategorizado Produto { get; set; }

        public ImagemCategorizado(string urlImagem, ProdutoCategorizado produto)
        {
            UrlImagem = urlImagem;
            Produto = produto;
        }

        public ImagemCategorizado()
        {

        }
    }
}
