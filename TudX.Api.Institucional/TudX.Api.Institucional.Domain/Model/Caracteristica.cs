﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Institucional.Domain.Model
{
    public class Caracteristica : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string Nome { get; set; }
        public virtual long IdentificadorCategoria { get; set; }
        public virtual Caracteristica CaracteristicaSuperior { get; set; }
        public virtual ICollection<Caracteristica> CaracteristicasFilhas { get; set; }

        public Caracteristica(string nome, long identificadorCategoria)
        {
            Nome = nome;
            IdentificadorCategoria = identificadorCategoria;
        }

        public Caracteristica()
        {

        }
    }
}
