﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Institucional.Domain.Model
{
    public class ProdutoCaracteristicaNaoCategorizada : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual CaracteristicaNaoCategorizada CaracteristicaNaoCategorizada { get; set; }
        public virtual long IdentificadorProdutoCategorizado { get; set; }
        public virtual DateTime DataInclusao { get; set; }
        
        public ProdutoCaracteristicaNaoCategorizada()
        {
            DataInclusao = DateTime.Now;
        }
    }
}
