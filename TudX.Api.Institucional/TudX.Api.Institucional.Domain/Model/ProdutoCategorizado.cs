﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Institucional.Domain.Model
{
    public class ProdutoCategorizado : IEntity
    {
        public virtual long Identificador { get; set; }
        public virtual string CodigoProduto { get; set; }
        public virtual string Nome { get; set; }
        public virtual string Descricao { get; set; }
        public virtual string UrlAnuncio { get; set; }
        public virtual decimal Valor { get; set; }
        public virtual string FormaPagamento { get; set; }
        public virtual long IdentificadorLoja { get; set; }
        public virtual long IdentificadorCategoria { get; set; }
        public virtual long IdentificadorProdutoCrawler { get; set; }
        public virtual ICollection<CaracteristicaCategorizado> Caracteristicas { get; set; }
        public virtual ICollection<ImagemCategorizado> Imagens { get; set; }

        public ProdutoCategorizado(string nome, string descricao, string urlAnuncio, decimal valor, string formaPagamento, long identificadorCategoria, long identificadorLoja)
        {
            Nome = nome;
            Descricao = descricao;
            UrlAnuncio = urlAnuncio;
            Valor = valor;
            FormaPagamento = formaPagamento;
            IdentificadorCategoria = identificadorCategoria;
            IdentificadorLoja = identificadorLoja;
        }

        public ProdutoCategorizado()
        {

        }
    }
}
