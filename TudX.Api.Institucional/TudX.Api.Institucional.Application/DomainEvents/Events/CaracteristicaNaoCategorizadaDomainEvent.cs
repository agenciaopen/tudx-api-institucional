﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Institucional.Application.DomainEvents.Events
{
    public class CaracteristicaNaoCategorizadaDomainEvent : INotification
    {
        public long IdentificadorProduto { get; set; }
        public long IdentificadorCategoria { get; set; }
        public string Nome { get; set; }
    }
}
