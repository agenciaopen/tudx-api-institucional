﻿using Mapster;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TudX.Api.Institucional.Application.DomainEvents.Events;
using TudX.Api.Institucional.Application.Services;
using TudX.Api.Institucional.CrossCuting.IntegrationEvents.Events;
using TudX.Api.Institucional.Domain.Model;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Institucional.Application.DomainEvents.EventHandling
{
    public class CaracteristicaNaoCategorizadaDomainEventHandler : INotificationHandler<CaracteristicaNaoCategorizadaDomainEvent>
    {
        private readonly CaracteristicaNaoCategorizadaService caracteristicaNaoCategorizadaService;
        private readonly ILogger _logger;
        private readonly IEventBus _eventBus;

        public CaracteristicaNaoCategorizadaDomainEventHandler(IServiceProvider serviceProvider,
                                                               ILoggerFactory loggerFactory,
                                                               IEventBus eventBus)
        {
            caracteristicaNaoCategorizadaService = new CaracteristicaNaoCategorizadaService(serviceProvider);
            _logger = loggerFactory?.CreateLogger<CaracteristicaNaoCategorizadaDomainEventHandler>() ?? throw new ArgumentNullException(nameof(loggerFactory));
            _eventBus = eventBus;
        }

        public async Task Handle(CaracteristicaNaoCategorizadaDomainEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                Caracteristica caracteristica = caracteristicaNaoCategorizadaService.Incluir(notification);
                if (caracteristica != null)
                {
                    CaracteristicaNaoCategorizadaAdicionadaIntegrationEvent caracteristicaNaoCategorizadaAdicionadaIntegrationEvent
                        = caracteristica.Adapt<CaracteristicaNaoCategorizadaAdicionadaIntegrationEvent>();

                    _eventBus.PublishAndRegisterEvent(caracteristicaNaoCategorizadaAdicionadaIntegrationEvent);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Ocorreu um erro ao processar a mensagem: {0}", ex.Message));
            }
        }
    }
}
