﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Application.Specifications;
using TudX.Api.Institucional.CrossCuting.Data;
using TudX.Api.Institucional.Domain.Model;
using TudX.Domain.Base;

namespace TudX.Api.Institucional.Application.Services
{
    public class ProdutoNaoCategorizadoService : DomainServiceRelationalBase<ProdutoNaoCategorizado>
    {
        public ProdutoNaoCategorizadoService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public void SalvarProdutoNaoCategorizado(ProdutoCrawlerData produtoCrawlerData)
        {
            ProdutoNaoCategorizadoPorLojaNomeEMigalhaSpecification spec 
                = new ProdutoNaoCategorizadoPorLojaNomeEMigalhaSpecification(produtoCrawlerData.IdentificadorLoja,
                                                                             produtoCrawlerData.Nome,
                                                                             produtoCrawlerData.NomeCategoria);

            ProdutoNaoCategorizado produtoNaoCategorizado = this.FindSingleBySpecification(spec);
            if (produtoNaoCategorizado == null)
                produtoNaoCategorizado = new ProdutoNaoCategorizado();

            produtoNaoCategorizado.IdentificadorLoja = produtoCrawlerData.IdentificadorLoja;
            produtoNaoCategorizado.Nome = produtoCrawlerData.Nome;
            produtoNaoCategorizado.MigalhaCategoria = produtoCrawlerData.NomeCategoria;
            produtoNaoCategorizado.UrlAnuncio = produtoCrawlerData.UrlAnuncio;
            if (produtoNaoCategorizado.Identificador > 0)
            {
                produtoNaoCategorizado.DataAlteracao = DateTime.Now;
            }
            else
            {
                produtoNaoCategorizado.DataInclusao = DateTime.Now;
            }

            this.Save(produtoNaoCategorizado);
            this.SaveChanges();
        }
    }
}
