﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TudX.Api.Institucional.Application.Specifications;
using TudX.Api.Institucional.CrossCuting.Data;
using TudX.Api.Institucional.Domain.Model;
using TudX.Domain.Base;
using TudX.Domain.Base.Repository;
using TudX.Exceptions;

namespace TudX.Api.Institucional.Application.Services
{
    public class CategoriaLojaService : DomainServiceRelationalBase<CategoriaLoja>
    {
        private CategoriaService categoriaService;

        public CategoriaLojaService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            categoriaService = new CategoriaService(serviceProvider);
        }

        public void SalvarCarga(CargaCategoriaLojaData cargaCategoriaLojaData)
        {
            CategoriaLoja categoriaLoja = new CategoriaLoja();            
            DeParaCategoriaLoja(categoriaLoja, cargaCategoriaLojaData.Categoria, cargaCategoriaLojaData.IdentificadorLoja);

            this.Save(categoriaLoja);
            this.SaveChanges();
        }

        public void DeParaCategoriaLoja(CategoriaLoja categoriaLoja, CategoriaLojaParaCargaData categoriaLojaData, long identificadorLoja)
        {
            categoriaLoja.IdentificadorLoja = identificadorLoja;
            Categoria categoriaEncontrada = RecuperarIdentificadorCategoriaTudx(null, categoriaLojaData.NomeCategoriaTudx);
            if (categoriaEncontrada == null)
                throw new BusinessException(string.Format("Categoria tudx não encontrada: {0}", categoriaLojaData.NomeCategoriaTudx));

            categoriaLoja.IdentificadorCategoria = categoriaEncontrada.Identificador;
            categoriaLoja.Nome = categoriaLojaData.Nome;
            
            if (categoriaLojaData.CategoriasFilhas != null)
            {
                foreach (var categoriaFilha in categoriaLojaData.CategoriasFilhas)
                {
                    CategoriaLoja categoriaLojaFilha = new CategoriaLoja();
                    if (categoriaLoja.CategoriasFilhas == null)
                        categoriaLoja.CategoriasFilhas = new List<CategoriaLoja>();

                    DeParaCategoriaLoja(categoriaLojaFilha, categoriaFilha, identificadorLoja);
                    categoriaLoja.CategoriasFilhas.Add(categoriaLojaFilha);
                }
            }
        }

        private Categoria RecuperarIdentificadorCategoriaTudx(Categoria categoria, string nomeCategoriaTudx)
        {
            List<string> nomeCategoriasTudx = nomeCategoriaTudx.Split('|').ToList();
            if (categoria == null)
                categoria = categoriaService.FindSingleBySpecification(new CategoriaMacroPorNomeSpecification(nomeCategoriasTudx.FirstOrDefault()));

            string ultimoNomeCategoria = nomeCategoriasTudx.LastOrDefault();

            if (categoria.Nome.ToUpper().Trim() == ultimoNomeCategoria.ToUpper().Trim())
                return categoria;

            if (categoria.CategoriasFilhas != null)
            {
                foreach (var categoriaFilha in categoria.CategoriasFilhas)
                {
                    categoria = RecuperarIdentificadorCategoriaTudx(categoriaFilha, nomeCategoriaTudx);

                    if (categoria != null)
                        return categoria;
                }
            }

            return null;
        }
    }
}
