﻿using Mapster;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.CrossCuting.Data;
using TudX.Api.Institucional.Domain.Model;
using TudX.Domain.Base;
using TudX.Exceptions;

namespace TudX.Api.Institucional.Application.Services
{
    public class LojaService : DomainServiceRelationalBase<Loja>
    {
        public LojaService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        public Loja Salvar(LojaData lojaData)
        {
            Loja loja = lojaData.Adapt<Loja>();
            this.Save(loja);
            this.SaveChanges();

            return loja;
        }

        public IEnumerable<LojaData> BuscarTodasAsLojas()
        {
            return this.FindAll().Adapt<List<LojaData>>();
        }

        public LojaData BuscarLojaPorIdentificador(long identificador)
        {
            Loja loja = this.FindById(identificador);

            if (loja == null)
                throw new BusinessException("Loja não encontrada.");

            return loja.Adapt<LojaData>();
        }
    }
}
