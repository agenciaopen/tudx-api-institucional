﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TudX.Adapter.MediatR;
using TudX.Api.Institucional.Application.DomainEvents.Events;
using TudX.Api.Institucional.Application.Specifications;
using TudX.Api.Institucional.CrossCuting.Data;
using TudX.Api.Institucional.Domain.Model;
using TudX.Domain.Base;
using TudX.Domain.Base.Repository;

namespace TudX.Api.Institucional.Application.Services
{
    public class ProdutoCategorizadoService : DomainServiceRelationalMediatRBase<ProdutoCategorizado>
    {
        private CategoriaLojaService categoriaLojaService;
        private CaracteristicaService caracteristicaService;
        private ProdutoNaoCategorizadoService produtoNaoCategorizadoService;

        public ProdutoCategorizadoService(IServiceProvider serviceProvider, IMediator mediator) : base(serviceProvider, mediator)
        {
            categoriaLojaService = new CategoriaLojaService(serviceProvider);
            caracteristicaService = new CaracteristicaService(serviceProvider);
            produtoNaoCategorizadoService = new ProdutoNaoCategorizadoService(serviceProvider);
        }

        /// <summary>
        /// Inclui o produto que foi crawleado.
        /// </summary>
        /// <param name="produtoCrawlerData">Produto crawleado.</param>
        /// <returns>Se ocorrer com sucesso, retorna o produto categorizado.</returns>
        public async Task<ProdutoCategorizado> IncluirProduto(ProdutoCrawlerData produtoCrawlerData)
        {
            try
            {
                ProdutoCategorizado produtoCategorizado = null;
                long identificadorCategoria = ObterCategoriaDoProdutoCrawleado(produtoCrawlerData.NomeCategoria);
                if (identificadorCategoria > 0)
                {
                    produtoCategorizado = DeParaProdutoCategorizado(produtoCrawlerData, identificadorCategoria);
                    this.Save(produtoCategorizado);
                    AtribuirIdentificadorDoProdutoNasCaracteristicasNaoCategorizadas(produtoCategorizado);                    
                    this.SaveChanges();
                }
                else
                {
                    produtoNaoCategorizadoService.SalvarProdutoNaoCategorizado(produtoCrawlerData);
                }

                return produtoCategorizado;
            }
            catch (Exception)
            {
                return null;
            }
        }
        
        /// <summary>
        /// Inclui o produto que foi crawleado.
        /// </summary>
        /// <param name="produtoCrawlerData">Produto crawleado.</param>
        /// <returns>Verdadeiro para produto alterado com sucesso.</returns>
        public async Task<ProdutoCategorizado> AlterarProduto(ProdutoCrawlerData produtoCrawlerData)
        {
            try
            {
                ProdutoCategorizado produtoCategorizado = null;
                long identificadorCategoria = ObterCategoriaDoProdutoCrawleado(produtoCrawlerData.NomeCategoria);
                if (identificadorCategoria > 0)
                {
                    produtoCategorizado = DeParaProdutoCategorizado(produtoCrawlerData, identificadorCategoria);
                    this.Save(produtoCategorizado);
                    this.SaveChanges();
                }
                else
                {
                    produtoNaoCategorizadoService.SalvarProdutoNaoCategorizado(produtoCrawlerData);
                }

                return produtoCategorizado;
            }
            catch (Exception)
            {
                return null;
            }
        }

        #region Métodos privados
        private void AtribuirIdentificadorDoProdutoNasCaracteristicasNaoCategorizadas(ProdutoCategorizado produtoCategorizado)
        {
            foreach (var domainEvent in DomainEvents)
            {
                if (domainEvent is CaracteristicaNaoCategorizadaDomainEvent)
                {
                    var caracteristica = domainEvent as CaracteristicaNaoCategorizadaDomainEvent;
                    caracteristica.IdentificadorProduto = produtoCategorizado.Identificador;
                }
            }
        }

        private long ObterCategoriaDoProdutoCrawleado(string nomeCategoria)
        {
            long identificadorReturn = 0;
            long? identificadorCategoriaSuperior = null;
            string[] categoriasCrawler = nomeCategoria.Split('|');
            foreach (var categoriaCrawler in categoriasCrawler)
            {
                CategoriaLoja categoriaLoja = categoriaLojaService.FindSingleBySpecification(new CategoriaLojaPorNomeSpecification(categoriaCrawler, identificadorCategoriaSuperior));
                if (categoriaLoja != null)
                {
                    identificadorReturn = categoriaLoja.IdentificadorCategoria;
                    identificadorCategoriaSuperior = categoriaLoja.IdentificadorSuperior;
                } else
                {
                    identificadorCategoriaSuperior = null;
                }
            }

            return identificadorReturn;
        }

        private ProdutoCategorizado DeParaProdutoCategorizado(ProdutoCrawlerData produtoCrawlerData, long identificadorCategoria)
        {
            ProdutoCategorizado produtoCategorizado 
                = this.FindSingleBySpecification(new ProdutoCategorizadoPorLojaNomeCodigoProdutoEUrlAnuncioSpecification(produtoCrawlerData.IdentificadorLoja,
                                                                                                                         produtoCrawlerData.Nome,
                                                                                                                         produtoCrawlerData.CodigoProduto,
                                                                                                                         produtoCrawlerData.UrlAnuncio));
            if (produtoCategorizado == null)
                produtoCategorizado = new ProdutoCategorizado();

            produtoCategorizado.IdentificadorLoja = produtoCrawlerData.IdentificadorLoja;
            produtoCategorizado.IdentificadorCategoria = identificadorCategoria;
            produtoCategorizado.CodigoProduto = produtoCrawlerData.CodigoProduto;
            produtoCategorizado.Descricao = produtoCrawlerData.Descricao;
            produtoCategorizado.FormaPagamento = produtoCrawlerData.FormaPagamento;
            produtoCategorizado.UrlAnuncio = produtoCrawlerData.UrlAnuncio;
            produtoCategorizado.Valor = produtoCrawlerData.Valor;
            produtoCategorizado.Nome = produtoCrawlerData.Nome;
            produtoCategorizado.IdentificadorProdutoCrawler = produtoCrawlerData.Identificador;

            if (produtoCrawlerData.Imagens != null)
                DeParaImagensCategorizadoImagensCrawleadas(produtoCategorizado, produtoCrawlerData.Imagens);

            if (produtoCrawlerData.Caracteristicas != null)
            {
                DeParaCaracteristicasCategorizadosCaracteristicasCrawleadas(identificadorCategoria,
                                                                            produtoCategorizado,
                                                                            produtoCrawlerData.Caracteristicas);
            }

            return produtoCategorizado;
        }
        
        private void DeParaCaracteristicasCategorizadosCaracteristicasCrawleadas(long identificadorCategoria,
                                                                                 ProdutoCategorizado produtoCategorizado,
                                                                                 ICollection<CaracteristicaCrawlerData> caracteristicas)
        {
            if (produtoCategorizado.Caracteristicas == null)
                produtoCategorizado.Caracteristicas = new List<CaracteristicaCategorizado>();

            foreach (var caracteristica in caracteristicas)
            {
                if (!produtoCategorizado.Caracteristicas.ToList().Exists(c => c.Nome.ToUpper() == caracteristica.Nome.ToUpper()))
                {
                    Caracteristica caracteristicaDB = caracteristicaService.FindSingleBySpecification(new CaracteristicaPorNomeECategoriaSpecification(caracteristica.Nome, identificadorCategoria));
                    if (caracteristicaDB != null)
                    {
                        produtoCategorizado.Caracteristicas.Add(new CaracteristicaCategorizado()
                        {
                            Nome = caracteristica.Nome,
                            IdentificadorCaracteristica = caracteristicaDB.Identificador,
                            Valor = caracteristica.Valor,
                            Produto = produtoCategorizado
                        });
                    }
                    else if (produtoCategorizado.Identificador <= 0)
                    {
                        CaracteristicaNaoCategorizadaDomainEvent caracteristicaNaoCategorizadaDomainEvent = new CaracteristicaNaoCategorizadaDomainEvent();
                        caracteristicaNaoCategorizadaDomainEvent.IdentificadorCategoria = identificadorCategoria;
                        caracteristicaNaoCategorizadaDomainEvent.Nome = caracteristica.Nome;                        
                        AddDomainEvent(caracteristicaNaoCategorizadaDomainEvent);
                    }
                }
            }
        }

        private void DeParaImagensCategorizadoImagensCrawleadas(ProdutoCategorizado produtoCategorizado, ICollection<ImagemCrawlerData> imagens)
        {
            if (produtoCategorizado.Imagens == null)
                produtoCategorizado.Imagens = new List<ImagemCategorizado>();

            foreach (var imagem in imagens)
            {
                if (!produtoCategorizado.Imagens.ToList().Exists(i => i.UrlImagem == imagem.UrlImagem))
                    produtoCategorizado.Imagens.Add(new ImagemCategorizado() { UrlImagem = imagem.UrlImagem, Produto = produtoCategorizado });
            }
        }
        #endregion
    }
}
