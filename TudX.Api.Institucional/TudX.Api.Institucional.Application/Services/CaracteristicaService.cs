﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Domain.Model;
using TudX.Domain.Base;
using TudX.Domain.Base.Repository;

namespace TudX.Api.Institucional.Application.Services
{
    public class CaracteristicaService : DomainServiceRelationalBase<Caracteristica>
    {
        public CaracteristicaService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }
    }
}
