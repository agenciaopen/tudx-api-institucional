﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Application.Specifications;
using TudX.Api.Institucional.CrossCuting.Data;
using TudX.Api.Institucional.Domain.Model;
using TudX.Domain.Base;
using TudX.Domain.Base.Repository;
using TudX.Exceptions;
using TudX.Specification.Business;

namespace TudX.Api.Institucional.Application.Services
{
    /// <summary>
    /// Classe de serviço da categoria.
    /// </summary>
    public class CategoriaService : DomainServiceRelationalBase<Categoria>
    {
        /// <summary>
        /// Construtor padrão
        /// </summary>        
        public CategoriaService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        /// <summary>
        /// Busca as categorias do Tudx. Nesta busca, há um controle de pai, para garantir que venha somente os pais, que vão trazer os filhos automaticamente
        /// </summary>
        /// <returns>Todas as categorias pais, com seus filhos, netos e etc.</returns>
        public IEnumerable<Categoria> BuscarTodasCategorias()
        {
            CategoriaPaiSpecification spec = new CategoriaPaiSpecification();
            spec.Pagination.Take = 5;

            var categorias = this.FindBySpecification(spec);

            return categorias;
        }

        /// <summary>
        /// Busca a categoria pelo o identificador
        /// </summary>
        /// <param name="identificador">Identificador da categoria</param>
        /// <returns>Categoria encontrada.</returns>
        public Categoria BuscarCategoriaPorIdentificador(long identificador)
        {
            Categoria categoria = this.FindById(identificador);

            if (categoria == null)
                throw new BusinessException("Categoria não encontrada");

            return categoria;
        }

        /// <summary>
        /// Salva categoria
        /// </summary>
        /// <param name="categoriaData">Informações da categoria</param>
        /// <returns>Categoria salva</returns>
        public Categoria Salvar(CategoriaParaSalvarData categoriaData)
        {
            Categoria categoria = null;
            if (categoriaData.Identificador > 0)
                categoria = this.FindById(categoriaData.Identificador);

            if (categoria == null)
                categoria = new Categoria();

            categoria.IdentificadorSuperior = categoriaData.IdentificadorSuperior;
            categoria.Nome = categoriaData.Nome;

            this.Save(categoria);
            this.SaveChanges();

            return categoria;
        }

        public Categoria SalvarCarga(Categoria categoria)
        {
            this.Save(categoria);
            this.SaveChanges();

            return categoria;
        }
    }
}
