﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Application.DomainEvents.Events;
using TudX.Api.Institucional.Application.Specifications;
using TudX.Api.Institucional.Domain.Model;
using TudX.Domain.Base;

namespace TudX.Api.Institucional.Application.Services
{
    public class CaracteristicaNaoCategorizadaService : DomainServiceRelationalBase<CaracteristicaNaoCategorizada>
    {
        private readonly CaracteristicaService caracteristicaService;

        public CaracteristicaNaoCategorizadaService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            caracteristicaService = new CaracteristicaService(serviceProvider);
        }

        public Caracteristica Incluir(CaracteristicaNaoCategorizadaDomainEvent caracteristicaNaoCategorizadaDomainEvent)
        {
            CaracteristicaNaoCategorizadaPorNomeECategoriaSpecification spec
                = new CaracteristicaNaoCategorizadaPorNomeECategoriaSpecification(caracteristicaNaoCategorizadaDomainEvent.IdentificadorCategoria,
                                                                                  caracteristicaNaoCategorizadaDomainEvent.Nome);

            CaracteristicaNaoCategorizada caracteristicaNaoCategorizada = this.FindSingleBySpecification(spec);
            if (caracteristicaNaoCategorizada == null)
            {
                caracteristicaNaoCategorizada = new CaracteristicaNaoCategorizada();
                caracteristicaNaoCategorizada.QuantidadeIncidencia = 1;
                caracteristicaNaoCategorizada.Nome = caracteristicaNaoCategorizadaDomainEvent.Nome;
                caracteristicaNaoCategorizada.IdentificadorCategoria = caracteristicaNaoCategorizadaDomainEvent.IdentificadorCategoria;
                caracteristicaNaoCategorizada.Produtos = new List<ProdutoCaracteristicaNaoCategorizada>();
                caracteristicaNaoCategorizada.Produtos.Add(new ProdutoCaracteristicaNaoCategorizada() {
                                                                IdentificadorProdutoCategorizado = caracteristicaNaoCategorizadaDomainEvent.IdentificadorProduto,
                                                                CaracteristicaNaoCategorizada = caracteristicaNaoCategorizada });
            }
            else
            {
                if (caracteristicaNaoCategorizada.FoiAdicionada)
                    return null;
                caracteristicaNaoCategorizada.QuantidadeIncidencia = caracteristicaNaoCategorizada.QuantidadeIncidencia + 1;
                caracteristicaNaoCategorizada.Produtos.Add(new ProdutoCaracteristicaNaoCategorizada()
                {
                    IdentificadorProdutoCategorizado = caracteristicaNaoCategorizadaDomainEvent.IdentificadorProduto,
                    CaracteristicaNaoCategorizada = caracteristicaNaoCategorizada
                });
            }

            this.Save(caracteristicaNaoCategorizada);            
            return ValidarSeCaracteristicaEhCandidataAVirarCaracteristicaDaCategoria(caracteristicaNaoCategorizada);
        }

        private Caracteristica ValidarSeCaracteristicaEhCandidataAVirarCaracteristicaDaCategoria(CaracteristicaNaoCategorizada caracteristicaNaoCategorizada)
        {
            if (caracteristicaNaoCategorizada.QuantidadeIncidencia > 5)
            {
                Caracteristica caracteristica 
                    = caracteristicaService.FindSingleBySpecification(new CaracteristicaPorNomeECategoriaSpecification(caracteristicaNaoCategorizada.Nome,
                                                                                                                       caracteristicaNaoCategorizada.IdentificadorCategoria));
                if (caracteristica == null)
                {
                    caracteristica = new Caracteristica();
                    caracteristica.IdentificadorCategoria = caracteristicaNaoCategorizada.IdentificadorCategoria;
                    caracteristica.Nome = caracteristicaNaoCategorizada.Nome;

                    caracteristicaService.Save(caracteristica);
                    caracteristicaNaoCategorizada.FoiAdicionada = true;
                    this.Save(caracteristicaNaoCategorizada);

                    return caracteristica;
                }
            }

            return null;
        }
    }
}
