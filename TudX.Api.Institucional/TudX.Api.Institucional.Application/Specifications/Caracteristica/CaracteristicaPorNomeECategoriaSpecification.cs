﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Institucional.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Institucional.Application.Specifications
{
    public class CaracteristicaPorNomeECategoriaSpecification : SpecificationBase<Caracteristica>
    {
        public string Nome { get; set; }
        public long IdentificadorCategoria { get; set; }

        public CaracteristicaPorNomeECategoriaSpecification(string nome, long identificadorCategoria)
        {
            Nome = nome;
            IdentificadorCategoria = identificadorCategoria;
        }

        public override Expression<Func<Caracteristica, bool>> SatisfiedBy()
        {
            return c => c.Nome.ToUpper() == Nome.ToUpper() && c.IdentificadorCategoria == IdentificadorCategoria;
        }
    }
}
