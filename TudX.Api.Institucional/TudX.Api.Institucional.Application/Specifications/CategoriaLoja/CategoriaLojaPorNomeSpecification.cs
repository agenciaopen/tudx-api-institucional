﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Institucional.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Institucional.Application.Specifications
{
    public class CategoriaLojaPorNomeSpecification : SpecificationBase<CategoriaLoja>
    {
        private long? IdentificadorSuperior { get; set; }

        private string Nome { get; set; }

        public CategoriaLojaPorNomeSpecification(string nome)
        {
            Nome = nome;
        }

        public CategoriaLojaPorNomeSpecification(string nome, long? identificadorSuperior)
        {
            Nome = nome;
            IdentificadorSuperior = identificadorSuperior;
        }

        public override Expression<Func<CategoriaLoja, bool>> SatisfiedBy()
        {
            AddExpression(Nome, cl => cl.Nome.Trim().ToUpper() == Nome.Trim().ToUpper());
            AddExpression(IdentificadorSuperior, cl => cl.IdentificadorSuperior == IdentificadorSuperior);

            return GetExpression();
        }
    }
}
