﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Institucional.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Institucional.Application.Specifications
{
    public class ProdutoCategorizadoPorLojaNomeCodigoProdutoEUrlAnuncioSpecification : SpecificationBase<ProdutoCategorizado>
    {
        private long identificadorLoja;

        private string nome;

        private string codigoProduto;

        private string urlAnuncio;

        public ProdutoCategorizadoPorLojaNomeCodigoProdutoEUrlAnuncioSpecification(long identificadorLoja, string nome, string codigoProduto, string urlAnuncio)
        {
            this.identificadorLoja = identificadorLoja;
            this.nome = nome;
            this.codigoProduto = codigoProduto;
            this.urlAnuncio = urlAnuncio;
        }

        public override Expression<Func<ProdutoCategorizado, bool>> SatisfiedBy()
        {
            return pc => pc.CodigoProduto == codigoProduto && pc.IdentificadorLoja == identificadorLoja && pc.Nome == nome && pc.UrlAnuncio == urlAnuncio;
        }
    }
}
