﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Institucional.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Institucional.Application.Specifications
{
    public class CategoriaPaiSpecification : SpecificationBase<Categoria>
    {
        public override Expression<Func<Categoria, bool>> SatisfiedBy()
        {
            return c => !c.IdentificadorSuperior.HasValue;
        }
    }
}
