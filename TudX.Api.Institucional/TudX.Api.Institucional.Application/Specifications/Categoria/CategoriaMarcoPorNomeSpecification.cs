﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Institucional.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Institucional.Application.Specifications
{
    public class CategoriaMacroPorNomeSpecification : SpecificationBase<Categoria>
    {
        private string nome;

        public CategoriaMacroPorNomeSpecification(string nome)
        {
            this.nome = nome;
        }

        public override Expression<Func<Categoria, bool>> SatisfiedBy()
        {
            return c => c.Nome.ToUpper().Trim() == nome.ToUpper().Trim() && !c.IdentificadorSuperior.HasValue;
        }
    }
}
