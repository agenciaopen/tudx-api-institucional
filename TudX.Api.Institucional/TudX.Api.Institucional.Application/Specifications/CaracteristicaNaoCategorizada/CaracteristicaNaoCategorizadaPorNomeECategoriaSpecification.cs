﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Institucional.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Institucional.Application.Specifications
{
    public class CaracteristicaNaoCategorizadaPorNomeECategoriaSpecification : SpecificationBase<CaracteristicaNaoCategorizada>
    {
        private long identificadorCategoria;
        private string nome;

        public CaracteristicaNaoCategorizadaPorNomeECategoriaSpecification(long identificadorCategoria, string nome)
        {
            this.identificadorCategoria = identificadorCategoria;
            this.nome = nome;
        }

        public override Expression<Func<CaracteristicaNaoCategorizada, bool>> SatisfiedBy()
        {
            return c => c.IdentificadorCategoria == identificadorCategoria && c.Nome.ToUpper().Trim() == nome.ToUpper().Trim();
        }
    }
}
