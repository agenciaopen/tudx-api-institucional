﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Institucional.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Institucional.Application.Specifications
{
    public class ProdutoNaoCategorizadoPorLojaNomeEMigalhaSpecification : SpecificationBase<ProdutoNaoCategorizado>
    {
        private long identificadorLoja;
        private string nome;
        private string migalha;

        public ProdutoNaoCategorizadoPorLojaNomeEMigalhaSpecification(long identificadorLoja, string nome, string migalha)
        {
            this.identificadorLoja = identificadorLoja;
            this.nome = nome;
            this.migalha = migalha;
        }

        public override Expression<Func<ProdutoNaoCategorizado, bool>> SatisfiedBy()
        {
            return p => p.IdentificadorLoja == identificadorLoja && p.Nome == nome && p.MigalhaCategoria == migalha;
        }
    }
}
