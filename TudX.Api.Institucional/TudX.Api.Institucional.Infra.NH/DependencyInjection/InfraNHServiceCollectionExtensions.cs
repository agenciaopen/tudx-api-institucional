﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using TudX.Adapter.NHibernate;
using TudX.Api.Institucional.Infra.NH.Mappings;
using TudX.Domain.Base.Repository;
using TudX.Repository.Base;
using TudX.Repository.Base.Provider;

namespace TudX.Api.Institucional.Infra.NH.DependencyInjection
{
    public static class InfraNHServiceCollectionExtensions
    {
        public static IServiceCollection AddNHAdapter(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped(typeof(IBaseRelationalRepository<>), typeof(BaseRelationalRepository<>));
            services.AddScoped(typeof(IBaseRepositoryRelationalProvider<>), typeof(NHibernateProvider<>));

            var connStr = configuration.GetConnectionString("ApiInstitucionalConnectionString");
            var _sessionFactory = Fluently.Configure()
                                      .Database(MsSqlConfiguration.MsSql2012.ConnectionString(connStr))
                                      .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(LojaMap))))
                                      .BuildSessionFactory();
            services.AddScoped(factory =>
            {
                return _sessionFactory.OpenSession();
            });

            return services;
        }
    }
}
