﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Domain.Model;

namespace TudX.Api.Institucional.Infra.NH.Mappings
{
    public class CaracteristicaNaoCategorizadaMap : ClassMap<CaracteristicaNaoCategorizada>
    {
        public CaracteristicaNaoCategorizadaMap()
        {
            Id(c => c.Identificador).GeneratedBy.Identity();
            Map(c => c.Nome);
            Map(c => c.QuantidadeIncidencia);
            Map(c => c.FoiAdicionada);
            Map(c => c.IdentificadorCategoria);

            HasMany(p => p.Produtos).KeyColumn("IdentificadorCaracteristicaNaoCategorizada").Cascade.AllDeleteOrphan();
        }
    }
}
