﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Domain.Model;

namespace TudX.Api.Institucional.Infra.NH.Mappings
{
    public class CategoriaMap : ClassMap<Categoria>
    {
        public CategoriaMap()
        {
            Id(c => c.Identificador).GeneratedBy.Identity();
            Map(c => c.Nome);
            Map(c => c.IdentificadorSuperior);
            HasMany(x => x.CategoriasFilhas).Cascade.SaveUpdate().KeyColumn("IdentificadorSuperior");
        }
    }
}
