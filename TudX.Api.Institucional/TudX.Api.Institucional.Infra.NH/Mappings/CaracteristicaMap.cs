﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Domain.Model;

namespace TudX.Api.Institucional.Infra.NH.Mappings
{
    public class CaracteristicaMap : ClassMap<Caracteristica>
    {
        public CaracteristicaMap()
        {
            Id(c => c.Identificador).GeneratedBy.Identity();
            Map(c => c.Nome);
            Map(c => c.IdentificadorCategoria);            
            References(c => c.CaracteristicaSuperior).Column("IdentificadorSuperior").Nullable();
            HasMany(x => x.CaracteristicasFilhas).Cascade.None().KeyColumn("IdentificadorSuperior");
        }
    }
}
