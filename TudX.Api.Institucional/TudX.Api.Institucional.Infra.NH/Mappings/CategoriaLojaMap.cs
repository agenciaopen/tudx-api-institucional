﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Domain.Model;

namespace TudX.Api.Institucional.Infra.NH.Mappings
{
    public class CategoriaLojaMap : ClassMap<CategoriaLoja>
    {
        public CategoriaLojaMap()
        {
            Id(c => c.Identificador).GeneratedBy.Identity();
            Map(c => c.Nome);
            Map(p => p.IdentificadorCategoria);
            Map(p => p.IdentificadorLoja);
            Map(c => c.IdentificadorSuperior);

            HasMany(x => x.CategoriasFilhas).Cascade.SaveUpdate().KeyColumn("IdentificadorSuperior");
        }
    }
}
