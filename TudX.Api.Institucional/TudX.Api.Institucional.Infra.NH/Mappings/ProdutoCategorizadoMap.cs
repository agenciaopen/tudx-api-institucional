﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Domain.Model;

namespace TudX.Api.Institucional.Infra.NH.Mappings
{
    public class ProdutoCategorizadoMap : ClassMap<ProdutoCategorizado>
    {
        public ProdutoCategorizadoMap()
        {
            Id(p => p.Identificador).GeneratedBy.Identity();
            Map(p => p.Nome);
            Map(p => p.CodigoProduto).Nullable();
            Map(p => p.Descricao).Length(100000);
            Map(p => p.FormaPagamento);
            Map(p => p.UrlAnuncio);
            Map(p => p.Valor);
            Map(p => p.IdentificadorCategoria);
            Map(p => p.IdentificadorLoja);
            Map(p => p.IdentificadorProdutoCrawler);

            HasMany(p => p.Caracteristicas).KeyColumn("IdentificadorProduto").Inverse().Cascade.AllDeleteOrphan();
            HasMany(p => p.Imagens).KeyColumn("IdentificadorProduto").Inverse().Cascade.AllDeleteOrphan();
        }
    }
}

