﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Domain.Model;

namespace TudX.Api.Institucional.Infra.NH.Mappings
{
    public class ProdutoNaoCategorizadoMap : ClassMap<ProdutoNaoCategorizado>
    {
        public ProdutoNaoCategorizadoMap()
        {
            Id(p => p.Identificador).GeneratedBy.Identity();
            Map(p => p.Nome);
            Map(p => p.IdentificadorLoja);
            Map(p => p.MigalhaCategoria);
            Map(p => p.DataInclusao);
            Map(p => p.UrlAnuncio);
            Map(p => p.DataAlteracao).Nullable();
        }
    }
}
