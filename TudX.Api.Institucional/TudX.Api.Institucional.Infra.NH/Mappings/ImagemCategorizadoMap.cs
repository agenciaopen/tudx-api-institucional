﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Domain.Model;

namespace TudX.Api.Institucional.Infra.NH.Mappings
{
    public class ImagemCategorizadoMap : ClassMap<ImagemCategorizado>
    {
        public ImagemCategorizadoMap()
        {
            Id(i => i.Identificador).GeneratedBy.Identity();
            Map(i => i.UrlImagem);            
            References(c => c.Produto).Column("IdentificadorProduto");
        }
    }
}
