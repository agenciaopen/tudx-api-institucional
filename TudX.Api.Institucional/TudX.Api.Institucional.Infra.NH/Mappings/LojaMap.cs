﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Domain.Model;

namespace TudX.Api.Institucional.Infra.NH.Mappings
{
    public class LojaMap : ClassMap<Loja>
    {
        public LojaMap()
        {
            Id(l => l.Identificador).GeneratedBy.Identity();
            Map(l => l.Nome);
            Map(l => l.UrlLoja);
            Map(l => l.UrlLogo);
        }
    }
}
