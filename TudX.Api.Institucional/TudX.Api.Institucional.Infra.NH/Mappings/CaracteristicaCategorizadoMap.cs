﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Domain.Model;

namespace TudX.Api.Institucional.Infra.NH.Mappings
{
    public class CaracteristicaCategorizadoMap : ClassMap<CaracteristicaCategorizado>
    {
        public CaracteristicaCategorizadoMap()
        {
            Id(c => c.Identificador).GeneratedBy.Identity();
            Map(c => c.Nome);
            Map(c => c.Valor).Nullable();
            Map(c => c.IdentificadorCaracteristica);            
            References(c => c.Produto).Column("IdentificadorProduto");
        }
    }
}
