﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Institucional.Domain.Model;

namespace TudX.Api.Institucional.Infra.NH.Mappings
{
    public class ProdutoCaracteristicaNaoCategorizadaMap : ClassMap<ProdutoCaracteristicaNaoCategorizada>
    {
        public ProdutoCaracteristicaNaoCategorizadaMap()
        {
            Id(p => p.Identificador).GeneratedBy.Identity();
            References(p => p.CaracteristicaNaoCategorizada).Column("IdentificadorCaracteristicaNaoCategorizada");
            Map(p => p.IdentificadorProdutoCategorizado);
            Map(p => p.DataInclusao);
        }
    }
}
